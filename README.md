### Requisitos
- Java Development Kit (JDK) 11 ou superior
- PostgreSQL 9.6 ou superior
- Maven 3.6.x ou superior

### Configuração do Banco de Dados
- Instale o PostgreSQL em sua máquina.
- Crie um banco de dados no PostgreSQL para o aplicativo.
- Abra o arquivo application.properties localizado na pasta src/main/resources.
- Edite as seguintes linhas de acordo com a configuração do seu banco de dados:

- spring.datasource.url=jdbc:postgresql://localhost:5432/nome_do_banco
- spring.datasource.username=seu_usuario
- spring.datasource.password=sua_senha

### Configuração do Ambiente de Desenvolvimento
- Clone este repositório para sua máquina.
- Abra um terminal na pasta raiz do projeto.
- Execute o comando mvn clean install para baixar as dependências e compilar o projeto.

### Executando o Aplicativo
- Após a conclusão da configuração do ambiente de desenvolvimento, execute o comando mvn spring-boot:run na pasta raiz do projeto.
- O aplicativo será iniciado e estará disponível no seguinte endereço: http://localhost:8080.



### Estrutura do Projeto
- src/main/java: Contém o código-fonte do aplicativo.
- src/main/resources: Contém os arquivos de configuração, como o arquivo application.properties para a configuração do banco de dados.
- src/test/java: Contém os testes automatizados do aplicativo.
- pom.xml: Arquivo de configuração do Maven, que gerencia as dependências e o build do projeto.

### Contribuindo
Se deseja contribuir com este projeto, sinta-se à vontade para abrir um "pull request" com suas alterações. Será um prazer receber colaborações para melhorar este aplicativo hospitalar em Java.

### Licença
Este projeto está licenciado sob a Licença MIT. Sinta-se à vontade para utilizar, modificar e distribuir o código-fonte conforme necessário.


### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
